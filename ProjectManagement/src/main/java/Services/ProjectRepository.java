/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Connection.Database;
import Models.Project;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author moonshine
 */
public class ProjectRepository extends Repository<Project> {
    
    public ProjectRepository(Database database) {
        super(database, database.projects);
    }
    
    public List<Project> getByTitle(String title) {
        Set<Project> projects = new HashSet<>();
        
        for(Project dbModel: set) {
            if(dbModel.title.contains(title)) {
                projects.add(dbModel);
            }
        }
        
        return new ArrayList<>(projects);
    }
    
    public List<Project> searchByDescription(String description) {
        Set<Project> projects = new HashSet<>();
        
        for(Project dbModel: set) {
            if(dbModel.description.contains(description)) {
                projects.add(dbModel);
            }
        }
        
        return new ArrayList<>(projects);
    }
    
}
