/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Connection.Database;
import Models.Issue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 *
 * @author moonshine
 */
public class IssueRepository extends Repository<Issue> {
    
    public IssueRepository(Database database) {
        super(database, database.issues);
    }
    
    public List<Issue> getByTitle(String title) {
        Set<Issue> projects = new HashSet<>();
        
        for(Issue dbModel: set) {
            if(dbModel.title.contains(title)) {
                projects.add(dbModel);
            }
        }
        
        return new ArrayList<>(projects);
    }
    
}
