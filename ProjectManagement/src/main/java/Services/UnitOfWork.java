/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Connection.Database;

/**
 *
 * @author moonshine
 */
public class UnitOfWork {
    
    public static UnitOfWork main() {
        if(_main == null) {
            _main = new UnitOfWork();
        }
        
        return _main;
    }
    
    private static UnitOfWork _main;
    
    private final Database database = Database.main();
    
    public ProjectRepository projectRepository() {
        if(_projectRepository == null) {
            _projectRepository = new ProjectRepository(database);
        }
        
        return _projectRepository;
    }
    
    public IssueRepository issueRepository() {
        if(_issueRepository == null) {
            _issueRepository = new IssueRepository(database);
        }
        
        return _issueRepository;
    }
    
    private ProjectRepository _projectRepository; 
    private IssueRepository _issueRepository;
    
    private UnitOfWork() { }
    
}
