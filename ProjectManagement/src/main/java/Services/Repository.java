/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Connection.Database;
import Models.Model;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author moonshine
 * @param <T> extends Model
 */
public class Repository<T extends Model>  {
    
    public Database database;
    
    public Set<T> set;
    
    public Repository(Database database, Set<T> set) {
        this.database = database;
        this.set = set;
    }
    
    public List<T> getAll() {
        return new ArrayList<T>(set);
    }
    
    public void create(T model) {
        set.add(model);
    }
    
    public void delete(T model) {
        set.remove(model);
    }
    
    public void update(T model) {
        T dbModel = getById(model.id);
        if(dbModel == null) {
            return;
        }
        
        dbModel.update(model);
    }
    
    public T getById(int id) {
        for(T model: set) {
            if(model.id == id) {
                return model;
            }
        }
        
        return null;
    }
    
}
