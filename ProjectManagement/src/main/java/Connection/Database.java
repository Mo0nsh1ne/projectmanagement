/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connection;

import Models.Issue;
import Models.Project;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author moonshine
 */
public class Database {
    
    public static Database main() {
        if(_main == null) {
            _main = new Database();
        }
        
        return _main;
    }
    
    private static Database _main;
    
    public Set<Project> projects = new HashSet<Project>();
    public Set<Issue> issues = new HashSet<Issue>();
    
    private Database() {
        Project project1 = new Project(1, "Galaxy Defender", "iOS video game");
        Project project2 = new Project(2, "Scrabble Defender", "Android video game");
        projects.add(project1);
        projects.add(project2);
        Issue issue1 = new Issue(1, "Galaxy Defender Issue", 100);
        issues.add(issue1);
    }
    
}
