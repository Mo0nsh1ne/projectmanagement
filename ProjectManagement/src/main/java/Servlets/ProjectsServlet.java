/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Models.Project;
import Services.ProjectRepository;
import Services.UnitOfWork;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author moonshine
 */
@WebServlet(name = "ProjectsServlet", urlPatterns = {"/projects"})
public class ProjectsServlet extends HttpServlet {
    
    private ProjectRepository repository = UnitOfWork.main().projectRepository();
    
    private String HOME = "projects/index.jsp";
    private String EDIT = "projects/edit.jsp";
    private String INFO = "projects/info.jsp";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        
        response.setContentType("text/html");  
        
        String path = HOME;
        
        if(action == null) {
            List<Project> projects = repository.getAll();
            System.out.println(projects.toArray().length);
            request.setAttribute("projects", projects.toArray());
        } else {
            if(action.equalsIgnoreCase("edit")) {
                int id = Integer.parseInt(request.getParameter("id"));
                Project project = repository.getById(id);
                request.setAttribute("project", project);
                path = EDIT;
            } else if(action.equalsIgnoreCase("delete")) {
                int id = Integer.parseInt(request.getParameter("id"));
                Project project = repository.getById(id);
                repository.delete(project);
            } else if(action.equalsIgnoreCase("info")) {
                int id = Integer.parseInt(request.getParameter("id"));
                Project project = repository.getById(id);
                request.setAttribute("project", project);
                path = INFO;
            } else if(action.equalsIgnoreCase("issues")) {
                int id = Integer.parseInt(request.getParameter("id"));
                Project project = repository.getById(id);
                request.setAttribute("issues", project.issues);
            }
        }
        
        RequestDispatcher rd = request.getRequestDispatcher(path);
        rd.forward(request,response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        
        Project project = new Project(id, title, description);
        if(repository.getAll().contains(project)) {
            repository.update(project);
        } else {
            repository.create(project);
        }
        
        RequestDispatcher rd = request.getRequestDispatcher(HOME);
        rd.forward(request,response);
    }
}
