/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Models.Issue;
import Models.Project;
import Services.IssueRepository;
import Services.ProjectRepository;
import Services.UnitOfWork;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author moonshine
 */
public class IssuesServlet extends HttpServlet {
    
    private IssueRepository repository = UnitOfWork.main().issueRepository();
    
    private String HOME = "issues/index.jsp";
    private String EDIT = "issues/edit.jsp";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        response.setContentType("text/html");  
        
        String path = HOME;
        
        if(action == null) {
            String stringId = request.getParameter("id");
            
            List<Issue> issues;
            
            if(stringId != null) {
                int id = Integer.parseInt(stringId);
                issues = UnitOfWork.main().projectRepository().getById(id).issues;
            } else {
                issues = repository.getAll();
            }
            
            request.setAttribute("issues", issues.toArray());
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            Issue issue = repository.getById(id);
            
            if(action.equalsIgnoreCase("edit")) {
                request.setAttribute("issue", issue);
                path = EDIT;
            } else if(action.equalsIgnoreCase("delete")) {
                repository.delete(issue);
            }
        }
        
        RequestDispatcher rd = request.getRequestDispatcher(path);
        rd.forward(request,response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        int priority = Integer.parseInt(request.getParameter("priority"));
        
        Issue issue = new Issue(id, title, priority);
        if(repository.getAll().contains(issue)) {
            repository.update(issue);
        } else {
            repository.create(issue);
        }
        
        RequestDispatcher rd = request.getRequestDispatcher(HOME);
        rd.forward(request, response);
    }

}
