/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author moonshine
 */
public class Issue extends Model<Issue> {
    
    public String title;
    public int priority;
    public boolean status = false;
    
    public Issue(int id, String title, int priority) {
        super(id);
        
        this.title = title;
        this.priority = priority;
    }
    
    @Override
    public void update(Issue model) {
        this.title = model.title;
        this.priority = model.priority;
        this.status = model.status;
    }
    
}
