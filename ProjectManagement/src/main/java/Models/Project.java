/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moonshine
 */
public class Project extends Model<Project> {
    
    public String title;
    public String description;
    public boolean status = false;
    
    public List<Issue> issues = new ArrayList<Issue>();
    
    public Project(int id, String title, String description) {
        super(id);
        
        this.title = title;
        this.description = description;
    }
    
    @Override
    public void update(Project model) {
        this.title = model.title;
        this.description = model.description;
        this.status = model.status;
    }
    
}
