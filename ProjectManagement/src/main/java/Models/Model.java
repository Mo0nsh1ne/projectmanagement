/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Connection.Database;

/**
 *
 * @author moonshine
 * @param <T> extends Model
 */
public class Model<T extends Model> {
    
    public int id;
    
    public static Database database = Database.main();
    
    public Model(int id) {
        this.id = id;
    }
    
    public void update(T model) { }
    
}
