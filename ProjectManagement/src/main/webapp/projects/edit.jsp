<%-- 
    Document   : edit
    Created on : Jan 18, 2021, 10:59:38 PM
    Author     : moonshine
--%>

<!DOCTYPE html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Project Manager</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href=".../css/nav-bar.css">
        
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src=".../js/nav-bar.js"></script>
    </head> 
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a href="#" class="navbar-brand">Project<b>Management</b></a>  		
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
		<span class="navbar-toggler-icon"></span>
            </button>
	
            <!-- Collection of nav links, forms, and other content for toggling -->
            <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
		<div class="navbar-nav">
		    <a href="<%= request.getContextPath() %>" class="nav-item nav-link">Home</a>
                    <a href="<%= request.getContextPath() %>/projects" class="nav-item nav-link">Projects</a>			
                    <a href="<%= request.getContextPath() %>/issues" class="nav-item nav-link">Issues</a>
		</div>
                
		<form class="navbar-form form-inline">
                    <div class="input-group search-box">								
                        <input type="text" id="search" class="form-control" placeholder="Search here...">
                        
                        <div class="input-group-append">
                            <span class="input-group-text">
				<i class="material-icons">&#xE8B6;</i>
                            </span>
			</div>
                    </div>
		</form>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-sm"></div>
                <div class="col-sm">
                    <form class="form-submit" action="<%= request.getContextPath() %>/projects/index.jsp" method="post">
                        <div class="form-group" >
                            <label for="title">Project name</label>
                            <input type="email" class="form-control" id="title" name="title" placeholder="Title">
                        </div>
                        <div class="form-group" >
                            <label for="description">Project description</label>
                            <input type="email" class="form-control" id="description" name="description" placeholder="Description">
                        </div>
                        <div class="form-group" >
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <div class="col-sm"></div>
            </div>
        </div>        
    </body>
</html>
